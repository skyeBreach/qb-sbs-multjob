local QBCore = exports['qb-core']:GetCoreObject()
local SHF = exports['qb-SBS-SHF']:GetClientObject()
local SHFDebug = SHF.Debug.Functions

RegisterNetEvent('QBCore:Client:OnPlayerLoaded', function()
    TriggerServerEvent("qb-multijob:server:PlayerLoadedIn")
end)

local function commandHandler()
    TriggerServerEvent("sBs-multijob:server:runcommand")
end

RegisterCommand(
	"multijob" --[[ string ]],
	commandHandler --[[ func ]],
	false --[[ boolean ]]
)

RegisterKeyMapping(
    "multijob",
    "Shows the multi job menu",
	'keyboard',
	'OEM_4'
)

--event handlers
RegisterNetEvent('qb-multijob:jobselectionmenu', function(cJobs)
    local jobChoices = {}
    local bAddUnemployed = false
    local bActiveUnemployed = true
    local player = QBCore.Functions.GetPlayerData()
    local unemployed = {}
    local totalJobs = #cJobs

    if (#cJobs < Config.maxAmountOfJobs) then
        bAddUnemployed = true
        totalJobs = totalJobs + 1
        unemployed = {
            ["citizenID"] = player.citizenid,
            ["jobID"] = #cJobs + 1,
            ["jobName"] = "unemployed",
            ["jobGrade"] = 0,
        }
        unemployed.jobData = QBCore.Shared.Jobs["unemployed"]
        if(player.job.name ~= "unemployed") then
            bActiveUnemployed = false
        end
    end

    local jobAmountString = "<br/><br/>Jobs Taken: " .. tostring(totalJobs) .. "/" .. tostring(Config.maxAmountOfJobs)

	jobChoices[#jobChoices + 1] = { header = "Multijob Menu", txt = "Select your job.".. jobAmountString , isMenuHeader = true }
	for k, job in pairs(cJobs) do
        local bDisabled = false
        if player.metadata["cJobID"] == job.jobID then
            bDisabled = true
        end
        if job and job.jobData then
            local grade = job.jobData.grades[tostring(job.jobGrade)]
            jobChoices[#jobChoices+1] = {header = job.jobData.label, txt =  "Grade: ".. grade.name .. ", Pay: $".. grade.payment, disabled = bDisabled, params = {event = "qb-multijob:SetJob", args = job}}
        end
	end
    if bAddUnemployed then
        local grade = unemployed.jobData.grades["0"]
        jobChoices[#jobChoices+1] = {header = "Civillian", text = "Grade: ".. grade.name .. ", Pay: $".. grade.payment, disabled = bActiveUnemployed, params = {event = "qb-multijob:SetJob", args = unemployed}}
    end
	exports['qb-menu']:openMenu(jobChoices)
end)

RegisterNetEvent('qb-multijob:SetJob', function(jobData)
    TriggerServerEvent("qb-multijob:server:SetJob", jobData)
end)