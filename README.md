# skyeBreach's Multi Job for Qbcore`
<<<<<<< HEAD

## Description
I made this script after seeing that people were charging ridiculous amounts of money for a simple script, and the free ones were too restrictive or out of date. This script is being worked and will remain free! Its a simple script that didn't take long to write, it allows switching between jobs, the max amount of jobs a person can take is configurable and there is no coded limit (although each job a person will have will increase the length of the database table).

<img src="https://i.imgur.com/Zc3sqzH.png" alt="" >

## Installation
To install this script clone the main branch and drop it into one of the resources folders. Once its been placed install the database table and if you have any plugins that add jobs using the QBCore export you must include this as a dependency otherwise the  plugin will not be able to see these new jobs.

The database command is held within the "database.sql" file, copy this script into the db,

```sql
	CREATE TABLE `multijob_joblist` (
	  `id` int(11) NOT NULL,
	  `citizenID` varchar(255) DEFAULT NULL,
	  `jobID` int(11) DEFAULT 1,
	  `jobName` varchar(255) DEFAULT NULL,
	  `jobGrade` int(11) DEFAULT 1
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

To add a dependency (any that adds jobs) edit this block as such,

```lua
	dependencies {
	  'qb-target',
	  'qb-menu',
	  'qb-smokepit-core', -- This is the added dependency
	}
```

## Usage
This menu can be opened with the rebind-able ']' key or by using the command "/multijob".

To configure the max amount of jobs a player can have edit this line in "config.lua",
```lua
	Config.maxAmountOfJobs = 5
```

## Support
If you need help with this script or any of my other scripts, please visit my discord server:
<div>
	<a href="https://discord.gg/YtTcJFh7Ke">
		<img src="https://discordapp.com/api/guilds/948435292984082472/widget.png?style=banner2" alt="" class="fr-fic fr-dii" width="265" height="63">
	</a>
</div>

If you wish to see my scripts in action i am primarily a developer for a FiveM community called **Smokepit-RP**, head on over to our server or join the discord to check it out:
<div>
	<a href="https://discord.gg/MdmgB5wk7J">
		<img src="https://discordapp.com/api/guilds/942907164488523826/widget.png?style=banner2" alt="" class="fr-fic fr-dii" width="265" height="63">
	</a>
</div>

Whilst I provide this script for free if you wish to support me you can head to my tebex store and donate for the script (This is not required by any means but does help a lot), https://skyebreachs-scripts.tebex.io/


## Roadmap
This script is being passively worked on as I have other scripts and projects within FiveM that have priority. This being said this script will receive periodic updates as I intend to implement the following:
- Improved UI using react, this will be completely optional
- More useful updating of stored jobs and implementation of net hooks to allow other scripts to attach onto these updates.
- Refactor and Optimisation

These updates along with this script will stay free!

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.

I use this license as word of mouth/general attribution aids me to get my work out there.
=======
>>>>>>> fecc0254df57ad3cb45392343c45b100920fe665

## Description

<img src="https://i.imgur.com/Zc3sqzH.png" alt="" >

## Showcase Video


## Installation
- Drag and drop 
- Setup DB Tale
- Need to set dependencies for any module that includes jobs (bc qb core is a back of shit)

## Usage
This menu can be opened with the rebind-able ']' key or by using the command "/multijob" 

## Support
If you need help with this script or any of my other scripts, please visit my discord server:
<div>
	<a href="https://discord.gg/YtTcJFh7Ke">
		<img src="https://discordapp.com/api/guilds/948435292984082472/widget.png?style=banner2" alt="" class="fr-fic fr-dii" width="265" height="63">
	</a>
</div>

If you wish to see my scripts in action i am primarily a developer for a FiveM community called **Smokepit-RP**, head on over to our server or join the discord to check it out:
<div>
	<a href="https://discord.gg/MdmgB5wk7J">
		<img src="https://discordapp.com/api/guilds/942907164488523826/widget.png?style=banner2" alt="" class="fr-fic fr-dii" width="265" height="63">
	</a>
</div>

Whilst I provide this script for free if you wish to support me you can head to my tebex store and donate for the script (This is not required by any means but does help a lot), https://skyebreachs-scripts.tebex.io/


## Roadmap
This script is being passively worked on as I have other scripts and projects within FiveM that have priority. This being said this script will receive periodic updates as I intend to implement the following:
- Improved UI using react, this will be completely optional
- More useful updating of stored jobs and implementation of net hooks to allow other scripts to attach onto these updates. 
- Refactor and Optimisation

These updates along with this script will stay free!

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.

I use this license as word of mouth/general attribution aids me to get my work out there.
