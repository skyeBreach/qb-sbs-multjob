
local QBCore = exports['qb-core']:GetCoreObject()
local SHF = exports['qb-SBS-SHF']:GetServerObject()

--This will is a basic thing and could be better

RegisterNetEvent("qb-multijob:server:PlayerLoadedIn")
AddEventHandler("qb-multijob:server:PlayerLoadedIn", function()
    local src = source
    local player = QBCore.Functions.GetPlayer(src)
    local playerData = player.PlayerData

    MySQL.Async.fetchAll('SELECT * FROM `multijob_joblist` WHERE citizenID = @cid', {['cid'] = playerData.citizenid} ,function(user)
        if(user == nil or user[1] == nil) then
            player.Functions.SetMetaData("cJobID", 1)
            MySQL.Async.insert('INSERT INTO `multijob_joblist` (citizenID, jobID, jobName, jobGrade) VALUES (@citizenID, @jobID, @jobName, @jobGrade)',
            {
            ['citizenID']   = playerData.citizenid,
            ['jobID']       = 1,
            ['jobName']     = playerData.job.name,
            ['jobGrade']    = playerData.job.grade.level,
            }, function() end)
        end
    end)
end)

RegisterNetEvent("sBs-multijob:server:runcommand",function()
    local src = source
    local cPlayer =  QBCore.Functions.GetPlayer(src)
    local playerData = cPlayer.PlayerData
    local jobID = playerData.metadata["cJobID"]
    local jobTable = {}

    MySQL.Async.fetchAll('SELECT * FROM `multijob_joblist` WHERE citizenID = @cid', {['cid'] = playerData.citizenid} ,function(jobs)
        if (jobs ~= nil) then
            -- check if jobID exists, if not add the current job
            local maxID = 0

            for k, job in ipairs(jobs) do
                if (jobID == job.jobID and playerData.job.name ~= job.jobName) then
                    job.jobName = playerData.job.name

                    MySQL.Sync.transaction({
                        'UPDATE `multijob_joblist` SET jobName = @jobName WHERE citizenID = @id And jobID = @jobID',
                    }, {
                        ['id'] =  playerData.citizenid,
                        ['jobID'] =  job.jobID,

                        ['jobName'] = job.jobName,
                    })
                end

                if (jobID == job.jobID and playerData.job.grade.level ~= job.jobGrade) then
                    job.jobGrade = playerData.job.grade.level

                    MySQL.Sync.transaction({
                        'UPDATE `multijob_joblist` SET jobGrade = @jobGrade WHERE citizenID = @id And jobID = @jobID',
                    }, {
                        ['id'] =  playerData.citizenid,
                        ['jobID'] =  job.jobID,

                        ['jobGrade'] = job.jobGrade,
                    }, function(success) end)
                end

                job["jobData"] = QBCore.Shared.Jobs[job.jobName]
                table.insert(jobTable, job.jobID, job)
                maxID = maxID + 1
            end

            local jobIDCheck = MySQL.Sync.fetchAll('SELECT * FROM `multijob_joblist` WHERE citizenID = @cid and jobID = @jobID', {['cid'] = playerData.citizenid, ['jobID'] = jobID })
            if ( jobIDCheck[1] == nil and playerData.job.name ~= "unemployed") then
                print("no db id, making one")
                local job = {
                    ['citizenID']   = playerData.citizenid,
                    ['jobID']       = maxID + 1,
                    ['jobName']     = playerData.job.name,
                    ['jobGrade']    = playerData.job.grade.level,

                }
                MySQL.Sync.insert('INSERT INTO `multijob_joblist` (citizenID, jobID, jobName, jobGrade) VALUES (@citizenID, @jobID, @jobName, @jobGrade)', job , function() end)
                job['jobData'] = QBCore.Shared.Jobs[playerData.job.name]

                table.insert(jobTable, job.jobID, job)
            end

            TriggerClientEvent('qb-multijob:jobselectionmenu', src, jobTable)
        end
    end)
end)

RegisterNetEvent("qb-multijob:server:SetJob", function(job)
    local src = source
    local cPlayer =  QBCore.Functions.GetPlayer(src)

    cPlayer.Functions.SetMetaData("cJobID", job.jobID)
    cPlayer.Functions.SetJob(job.jobName, job.jobGrade)

end)
