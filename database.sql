CREATE TABLE `multijob_joblist` (
  `id` int(11) NOT NULL,
  `citizenID` varchar(255) DEFAULT NULL,
  `jobID` int(11) DEFAULT 1,
  `jobName` varchar(255) DEFAULT NULL,
  `jobGrade` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;