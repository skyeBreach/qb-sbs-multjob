fx_version 'cerulean'
game 'gta5'
lua54 'yes'

description 'Simple multijob script designed for smokepit'
author "SkyeBreach"
version '1.0.0'

games {
  "gta5"
}

--ui_page 'web/build/index.html'
dependencies {
  'qb-target',
  'qb-menu',
  'qb-smokepit-core',
}

client_script "client/**/*"

server_scripts {
  '@oxmysql/lib/MySQL.lua',
  "server/**/*",
}

shared_scripts {
  'config.lua'
}

files {
}
escrow_ignore {
	"config.lua"
}

lua54 'yes'

